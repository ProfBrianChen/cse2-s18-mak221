///////////////////////////////
/// Card Generator LAB04
/// Marta Kasica-Soltan
/// February 16, 2018
/// CSE2S-210
/// Generates a random card from a deck

public class CardGenerator{
  // Main method required for every Java program
  public static void main(String [] args) {
    // Initializes and calculates random number 1-52
    int card = (int)((Math.floor(Math.random()*52))+1);
    String cardNumber;  // Type of card
    String cardSuit = "myString";  // Suit of card
    // Each suit has 13 cards, split this into four groups
    // Assign diamonds to first set
    if (card <= 13) {
      cardSuit = "Diamonds";
    }
    // Continue assigning, clubs
    if (card <= 26 && card > 13) {
      cardSuit = "Clubs";
    }
    // Continue assigning, hearts
    if (card <= 39 && card > 26) {
      cardSuit = "Hearts";
    }
    // Continue assigning, spades
    if (card > 39) {
      cardSuit = "Spades";
    }
    // Scale down 1-52 into 1-13 over next 3 if statements
    // This allows dealing with 13 possibilities instead of 52
    if (card > 13 && card < 27) {
      card = card-13;
    }
    // Continue scaling
    if (card > 26 && card < 40) {
      card = card-26;
    }
    // Continue scaling
    if (card > 39) {
      card = card-39;
    }
    // Use switch statement to assign a value to cardNumber based on its case
    // Use card terms such as "ace" and "king"
    // Account for every case
    switch (card) {
      case 1: cardNumber = "Ace";
              break;
      case 2: cardNumber = "2";
              break;
      case 3: cardNumber = "3";
              break;
      case 4: cardNumber = "4";
              break;
      case 5: cardNumber = "5";
              break;
      case 6: cardNumber = "6";
              break;
      case 7: cardNumber = "7";
              break;
      case 8: cardNumber = "8";
              break;
      case 9: cardNumber = "9";
              break;
      case 10: cardNumber = "10";
              break;
      case 11: cardNumber = "Jack";
              break;
      case 12: cardNumber = "Queen";
              break;
      case 13: cardNumber = "King";
              break;
      default: cardNumber = "Invalid number";
              break;
    }
    // Print the randomly generated suit and type combination
    System.out.println("You picked the "+(cardNumber)+" of "+(cardSuit));
  }
}