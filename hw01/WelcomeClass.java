//////////////////////////////
/// Welcome Class HW01
/// Marta Kasica-Soltan
/// January 26, 2018
/// CSE2S-210
public class WelcomeClass {
  public static void main(String [] args) {
    // Prints the welcome screen with my personal Lehigh network ID as a signature
    System.out.println("  -----------\n  | WELCOME |\n  ----------- \n  ^  ^  ^  ^  ^  ^ \n / \\/ \\/ \\/ \\/ \\/ \\\n<-M--A--K--2--2--1-> \n \\ /\\ /\\ /\\ /\\ /\\ /  \n  v  v  v  v  v  v");
    // Prints a short autobiography
    System.out.print("\nMy name is Marta Kasica-Soltan.  I'm a freshman at Lehigh and I'm 19 years old.  I'm majoring in computer engineering with a minor in psych and cog sci.  I have 23 dogs and like to dance, and I also play percussion in the Marching 97.");
  }
}