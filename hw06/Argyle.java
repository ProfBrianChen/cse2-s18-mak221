/// Hw06 HW06
/// Marta Kasica-Soltan
/// March 9, 2018
/// CSE2S-210
/// Prints out argyle pattern based on user input

import java.util.Scanner;  // Import scanner class
public class Argyle {
  public static void main(String[] args) {
    // Initialize and name scanner as myScanner
    Scanner myScanner = new Scanner(System.in);
    int windowWidth = 0;  // The width the user would like the argyle
    int test = 0;  // Useless variable, used for testing validity of user input
    // Ask user for an input that is a positive integer. If it doesn't abide, ask again
    do {
      System.out.print("Please enter a positive integer for the width of viewing window in characters: ");
      while (!myScanner.hasNextInt()) {  // Given the input is not an integer...
        String junkWord = myScanner.next();  // Clear scanner input to allow for another
        // Print an error, ask again for the input
        System.out.print("Error. Please enter a positive integer for the width of viewing window in characters:  ");
      }
      test = myScanner.nextInt();  // File input as test variable
    } while (test <= 0);  // While test is less than or equal to zero, ask again for input
    windowWidth = test;  // When test passes the test /hah/ set that value as the window width
    int windowLength = 0;  // Length of viewing window
    // Refer to above do while loop, except for the window's length
    do {
      System.out.print("Please enter a positive integer for the length of viewing window in characters: ");
      while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Please enter a positive integer for the length of viewing window in characters:  ");
      }
      test = myScanner.nextInt();
    } while (test <= 0);
    windowLength = test;  // Set value, if it passes, as prompted value
    int diamondWidth = 0;  // Width of each repeating diamond
    // Refer to the first do while loop, except this is for diamond width
    do {
      System.out.print("Please enter a positive integer for the width of the argyle diamonds: ");
      while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Please enter a positive integer for the width of the argyle diamonds:  ");
      }
      test = myScanner.nextInt();
    } while (test <= 0);
    diamondWidth = test;  // Set value, if it passes, as prompted value
    int stripeWidth = 0;  // Width of the center stripe
    // Refer to the first do while loop, except with stripe width, and this has some extra parameters
    do {
      System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: ");
      while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("Error. Please enter a positive odd integer for the width of the argyle center stripe:   ");
      }
      test = myScanner.nextInt();
    } while (test <= 0 || test > diamondWidth/2 || (test%2 != 1));  // Input must be greater than half diamond width
                                                                    // and an odd integer. If not, do repeats
    stripeWidth = test;  // Set value, if it passes, as prompted value
    // Ask user for three character inputs and store them
    System.out.print("Please enter a first character for pattern fill: ");
    String temp = myScanner.next();
    char firstChar = temp.charAt(0);  // Store the first character
    System.out.print("Please enter a second character for pattern fill: ");
    temp = myScanner.next();
    char secondChar = temp.charAt(0);  // Store the second character
    System.out.print("Please enter a third character for stripe fill: ");
    temp = myScanner.next();
    char thirdChar = temp.charAt(0);  // Store the third character
    // Regardless of how much the user enters, the only thing stored will be whatever the user input first
      ////////////////////////////////
      /// first and second pattern ///
      ////////////////////////////////
    String firstPattern = "";  // First pattern, empty
    String secondPattern = "";  // Second pattern, empty
    String secondPatternREAL = "";  // Combination of all rows for first and second
    String thirdPattern = "";  // Third pattern, empty
    String thirdPatternREAL = "";  // Combination of all rows for fourth and third
    String fourthPattern = "";  // Fourth pattern, empty
    String finalPattern = "";  // Combination of all four, empty
    String finalPatternREAL = "";  // Combination of all four, cut to proper length
    int spaceCounter = 0;  // Counter for which space you are in a line
    int stripeMath = (stripeWidth-1)/2;  // Value used very often, half of stripe width minus 1. Good for comparisons
    int stripeMath2 = stripeMath;  // Used since stripeMath could be modified
    int stripeMath3 = stripeMath;  // Used since stripeMath2 could be modified
    int fillCounter = 0;  // Counting how many spaces have been filled by first character.  Not diamond, not stripe
    int fillCounter2 = 0;  // Used since fillCounter could be modified
    int fillCounter3 = 0;  // Used since fillCounter2 could be modified
    int diamondCounter = 0;  // Counts how many spaces have been filled in a diamond
    int diamondCounter2 = 0;  // Diamond left to fill
    int diamondCounter3 = 0;  // Used since diamondCounter2 could be modified
    int fillerCounter = diamondWidth;  // Set to diamondWidth for comparison /see below/
    int fillerCounter2 = 0;  // Used to compare width of diamond to what has been filled by firstChar
    int lineCounter = 0;  // Counts which line the loop is on
    // Given the lines we have are less than the input window length, create the same argyle diamond again
    // Each time this while loop goes, a diamond of selected size is created
    while (lineCounter < windowLength){  
      // Assure following ints or strings are the same as were when initialized every time the loop goes again
      firstPattern = "";
      secondPattern = "";
      secondPatternREAL = "";
      thirdPattern = "";
      thirdPatternREAL = "";
      fourthPattern = "";
      fillerCounter = diamondWidth;
      stripeMath = stripeMath3;
      spaceCounter = 0;
      stripeMath = (stripeWidth-1)/2;
      stripeMath2 = stripeMath;
      stripeMath3 = stripeMath;
      fillCounter = 0;
      fillCounter2 = 0;
      fillCounter3 = 0;
      diamondCounter = 0;
      diamondCounter2 = 0;
      diamondCounter3 = 0;
      fillerCounter2 = 0;
    // FOR1 //
    // Use this for making each row in a diamond pattern. While it's smaller than it should be, continue
    // through loop again. Increment by 1 each time
    for (int rowCounter = 0; rowCounter < diamondWidth; rowCounter++){
      diamondCounter2 = diamondWidth-diamondCounter;  // Diamond left to fill
      firstPattern = "";  // Assure empty each time going through loop
      secondPattern = "";  // Assure empty each time going through loop
      // WHILE1 //
      // Adding the stripe before the main stripe for upper rows
      while (spaceCounter < rowCounter && rowCounter <= stripeMath){
        firstPattern += thirdChar;  // Add stripe character to first pattern
        // Whenever anything is added to first pattern, given that second pattern is the inverse, line by
        // line, that will be added to the end of the second pattern
        secondPattern = thirdChar + secondPattern; 
        spaceCounter++;  // This will increment every time any character is added
      }
      // IF1 //
      // Whenever the space you're in is equivalent to the row you're on, there will be the x
      if (spaceCounter == rowCounter){
        firstPattern += thirdChar;
        secondPattern = thirdChar + secondPattern;
        spaceCounter++;
      }
      // WHILE2 // 
      // Adding the rest of the stripe
      while (stripeMath > 0 && rowCounter <= stripeMath3){
        firstPattern += thirdChar;
        secondPattern = thirdChar + secondPattern;
        stripeMath--;  // Decrement this so you end up adding the second half of the stripe only
        spaceCounter++; 
      }
      // IF2 // 
      // Finishing off upper lines with the fill character
      if (rowCounter <= stripeMath3){
        // FOR2 //
        // h will be how many spaces there are to fill
        for (int h = diamondWidth-spaceCounter-diamondCounter; h > 0; h--){ 
          firstPattern += firstChar;
          secondPattern = firstChar + secondPattern;
          spaceCounter++;
        }
      }
      // IF3 //
      // Filling before the stripe
      if (spaceCounter == 0){
        fillCounter++;
        fillCounter2 = fillCounter;  // Since one will be changed
        // WHILE3 //
        // If filling is still necessary, it'll be greater than zero and less than the diamond left to fill
        while (fillCounter2 > 0 && fillCounter2 < diamondWidth - diamondCounter){
          firstPattern += firstChar;
          secondPattern = firstChar + secondPattern;
          fillCounter2--;  // Decrement in order to fill what's necessary
          spaceCounter++;
        }
        // IF4 //
        // Filler pre-stripe but post-diamond-hitting-stripe
        if (diamondCounter - stripeMath3 >= fillerCounter && spaceCounter <= diamondCounter){
          fillerCounter2 = fillerCounter;
          // WHILE4 //
          // Add fill, decrement each time. 
          while (fillerCounter2 > 0){
            firstPattern += firstChar;
            secondPattern = firstChar + secondPattern;
            spaceCounter++;
            fillerCounter2--;
          }
        }
      }
      // IF5 //
      // Filling the diamond pre-stripe
      if (diamondCounter + spaceCounter >= diamondWidth){
        // FOR3 //
        // d will be the width the diamond should be subtracted by what's already filling it
        // This will be filled by the diamond fill character
        for (int d = diamondWidth - (spaceCounter*2+stripeMath3); d > 0; d--){
          firstPattern += secondChar;
          secondPattern = secondChar + secondPattern;
          spaceCounter++;
        }
      }
      // IF6 //
      // Given this is an upper row but not the top row
      if (rowCounter <= stripeMath3 && rowCounter > 0){
        // FOR4 //
        // Add the diamond character to the row
        for (int p = 0; p >= 0; p++){
          firstPattern += secondChar;
          secondPattern = secondChar + secondPattern;
          spaceCounter++;
          // IF7 //
          // Break if the space you're on is greater than or = to how long the diamond should be
          if (spaceCounter >= diamondWidth){
            break;
          }
        }
      }
      // IF8 //
      // Print stripe if the space you're on is less than how wide the diamond should be and...
      if (spaceCounter < diamondWidth){
        // FOR5 //
        // as long as j, the decrementing stripewidth, lives
        for (int j = stripeWidth; j > 0; j--){
          firstPattern += thirdChar;
          secondPattern = thirdChar + secondPattern;
          spaceCounter++;
          // IF9 //
          // Break as soon as the space you're on equals the diamond's width
          if (spaceCounter >= diamondWidth){
            break;
          }
        }
        // IF10 //
        // Print fill after stripe
        if (spaceCounter == rowCounter + stripeMath + 1){
          // FOR6 //
          // g is the space left to fill with the fill character, decrementing each time the for loop goes
          for (int g = diamondWidth - diamondCounter - spaceCounter; g > 0; g--){
            firstPattern += firstChar;
            secondPattern = firstChar + secondPattern;
            spaceCounter++;
          }
        }
        // FOR7 //
        // k is how many diamond characters there are
        for (int k = diamondCounter; k > 0; k--){
          // IF11 //
          // If the spaces equal the diamond width, break
          if (spaceCounter >= diamondWidth){
            break;
          }
          // Add diamond fill to account for how much diamond there should be
          firstPattern += secondChar;
          secondPattern = secondChar + secondPattern;
          spaceCounter++;
        }
      }
      firstPattern += secondPattern;  // Combine the patterns
      // WHILE5 //
      // Continue adding the entire pattern on a line until it surpasses the width
      while (firstPattern.length() < windowWidth){
        firstPattern += firstPattern;
      }
      // Cut off the remainder of the pattern that isn't asked for using the windowWidth input
      firstPattern = firstPattern.substring(0, windowWidth);
      firstPattern += "\n";  // Add a new line to the end
      secondPatternREAL = secondPatternREAL + firstPattern;  // Add this line to the rest of the lines
      spaceCounter = 0;  // Re-initialize to zero
      stripeMath = stripeMath3;  // Re-initialize
      diamondCounter++;  // The size of the diamond increases by one each line
      fillerCounter--;  // The size of the fill decreases one each line
      lineCounter++;  // The line increases by one each line
    }
      ////////////////////////////////
      /// third and fourth pattern ///
      ////////////////////////////////
    // Note that, since the third and fourth patterns are exactly the first and second
    // but with different characters, the first pattern matches with the fourth and the
    // third matches the second. They're each one quarter of the diamond. 
    // Given this, the following code is exactly like the above, so each for/while/if was
    // given a number above, and will be referenced.
      
    // Assure these variables are initialized the same way
    fillerCounter = diamondWidth;
    stripeMath = stripeMath3;
    spaceCounter = 0;
    stripeMath = (stripeWidth-1)/2;
    stripeMath2 = stripeMath;
    stripeMath3 = stripeMath;
    fillCounter = 0;
    fillCounter2 = 0;
    fillCounter3 = 0;
    diamondCounter = 0;
    diamondCounter2 = 0;
    diamondCounter3 = 0;
    fillerCounter2 = 0;
    // Refer to FOR1
    for (int rowCounter = 0; rowCounter < diamondWidth; rowCounter++){
      diamondCounter2 = diamondWidth-diamondCounter;
      thirdPattern = "";
      fourthPattern = "";
      // Refer to WHILE1
      while (spaceCounter < rowCounter && rowCounter <= stripeMath){
        thirdPattern = thirdChar + thirdPattern;
        fourthPattern += thirdChar;
        spaceCounter++;
      }
      // Refer to IF1
      if (spaceCounter == rowCounter){
        thirdPattern = thirdChar + thirdPattern;
        fourthPattern += thirdChar;
        spaceCounter++;
      }
      // Refer to WHILE2
      while (stripeMath > 0 && rowCounter <= stripeMath3){
        thirdPattern = thirdChar + thirdPattern;
        fourthPattern += thirdChar;
        stripeMath--;
        spaceCounter++; 
      }
      // Refer to IF2
      if (rowCounter <= stripeMath3){
        // Refer to FOR2
        for (int h = diamondWidth-spaceCounter-diamondCounter; h > 0; h--){
          thirdPattern = secondChar + thirdPattern;
          fourthPattern += secondChar;
          spaceCounter++;
        }
      }
      // Refer to IF3
      if (spaceCounter == 0){
        fillCounter++;
        fillCounter2 = fillCounter;
        // Refer to WHILE3
        while (fillCounter2 > 0 && fillCounter2 < diamondWidth - diamondCounter){
          thirdPattern = secondChar + thirdPattern;
          fourthPattern += secondChar;
          fillCounter2--;
          spaceCounter++;
        }
        // Refer to IF4
        if (diamondCounter - stripeMath3 >= fillerCounter && spaceCounter <= diamondCounter){
          fillerCounter2 = fillerCounter;
          // Refer to WHILE4
          while (fillerCounter2 > 0){
            thirdPattern = secondChar + thirdPattern;
            fourthPattern += secondChar;
            spaceCounter++;
            fillerCounter2--;
          }
        }
      }
      // Refer to IF5
      if (diamondCounter + spaceCounter >= diamondWidth){
        // Refer to FOR3
        for (int d = diamondWidth - (spaceCounter*2+stripeMath3); d > 0; d--){
          thirdPattern = firstChar + thirdPattern;
          fourthPattern += firstChar;
          spaceCounter++;
        }
      }
      // Refer to IF6
      if (rowCounter <= stripeMath3 && rowCounter > 0){
        // Refer to FOR4
        for (int p = 0; p >= 0; p++){
          thirdPattern = firstChar + thirdPattern;
          fourthPattern += firstChar;
          spaceCounter++;
          // Refer to IF7
          if (spaceCounter >= diamondWidth){
            break;
          }
        }
      }
      // Refer to IF8
      if (spaceCounter < diamondWidth){
        // Refer to FOR5
        for (int j = stripeWidth; j > 0; j--){
          thirdPattern = thirdChar + thirdPattern;
          fourthPattern += thirdChar;
          spaceCounter++;
          // Refer to IF9
          if (spaceCounter >= diamondWidth){
            break;
          }
        }
        // Refer to IF10
        if (spaceCounter == rowCounter + stripeMath + 1){
          // Refer to FOR6
          for (int g = diamondWidth - diamondCounter - spaceCounter; g > 0; g--){
            thirdPattern = secondChar + thirdPattern;
            fourthPattern += secondChar;
            spaceCounter++;
          }
        }
        // Refer to FOR7
        for (int k = diamondCounter; k > 0; k--){
          // Refer to IF11
          if (spaceCounter >= diamondWidth){
            break;
          }
          thirdPattern = firstChar + thirdPattern;
          fourthPattern += firstChar;
          spaceCounter++;
        }
      }
      thirdPattern += fourthPattern;  // Combine the patterns
      // Refer to WHILE5
      while (thirdPattern.length() < windowWidth){
        thirdPattern += thirdPattern;
      }
      // Cut off the remainder of the pattern that isn't asked for using the windowWidth input
      thirdPattern = thirdPattern.substring(0, windowWidth);
      thirdPattern += "\n";  // Add a new line to the end
      thirdPatternREAL = thirdPatternREAL + thirdPattern;  // Add this line to the rest of the lines
      spaceCounter = 0;  // Re-initialize to zero
      stripeMath = stripeMath3;  // Re-initialize
      diamondCounter++;  // The size of the diamond increases by one each line
      fillerCounter--;  // The size of the fill decreases one each line
      lineCounter++;  // The line increases by one each line
    }
    finalPattern = (secondPatternREAL+thirdPatternREAL);  // Add the two halves
      finalPatternREAL += finalPattern;  // Add this to the whole thing
    }
      // Cut off the piece of the string that is going to be less than the length desired, found through
      // multiplying the window length and the window width (with some modification) 
      finalPatternREAL = finalPatternREAL.substring(0, (windowWidth+1)*windowLength);
      System.out.print(finalPatternREAL);  // Print the final desired argyle.
  }
}