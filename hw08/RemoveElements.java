/// RemoveElement HW08
/// Marta Kasica-Soltan
/// April 6, 2018
/// CSE2S-210
/// Removes an integer from a list of random numbers and then removes the value you want it to

import java.util.Scanner;  // Import scanner class
public class RemoveElements{
  // Generates an array of 10 random ints btwn 0 and 9 inclusive
  // Returns array
  public static int [] randomInput(){
    int array[] = new int[10];  // Allocate space for 10 ints
		// Assort 10 random integers
    for (int a = 0; a < 10; a++){
      array[a] = (int)(Math.random()*9.99);  // Will never = 10 but almost equal chance for 9
    }
    return array;  // Return new random array
  }
  // Creates a new array with one number fewer than list, composed
  // of same numbers except for whatever is in pos
  public static int [] delete(int list[], int pos){
		int newList[] = new int[list.length-1];  // Array has one less value
		int b = 0;  // Counter to count how many values have been put in new array
		// If it's in range,
    if (pos < 10 && pos >= 0){
			// Copy over every value that isn't the user-selected slot
      for (int a = 0; a < list.length; a++){
        if (a != pos){
					newList[b] = list[a];  // B only increments as values are added
					b++;  // Increment
				}
      }
    }
		// Otherwise, print out that the given number isn't valid
    else {
      System.out.println("The index is not valid.");
			newList = list;  // Set return value to the input
    }
    return newList;  // Return the new array
  }
  // Removes all elements equal to the target
  // Otherwise it returns an identical array
  public static int [] remove(int list[], int target){
		int valueCounter = 0;  // Count how many to remove
    boolean wasFound = false;  // Set true if target is found
		for (int z = 0; z < list.length; z++){
			// Count how many values are the input so you can remove them
			if (list[z] == target){
				valueCounter++;
				wasFound = true;  // Set to true when found
			}
		}
		int b = 0;  // Increment only when something is added to the new array
		int newList[] = new int[list.length-valueCounter];  // Make array big enough (subtract value)
		for (int a = 0; a < list.length; a++){
			// When the value isn't the target value, add it to the new array
			if (list[a] != target) {
				newList[b] = list[a];
				b++;  // Make sure you increment so you know how many are in new array
			}
		}
		// If the element was found, print message indicating so
		if(wasFound == true){
			System.out.println("Element "+target+" has been found.");
		}
		// If element not found, print message indicating so
		else {
			System.out.println("Element "+target+" was not found.");
		}
		return newList;  // Returns new array
  }
	// Chen's code
  public static void main(String [] args){
	Scanner myScanner = new Scanner(System.in);
  int num[] = new int[10];
  int newArray1[];
  int newArray2[];
  int index,target;
	String answer = "";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = myScanner.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = myScanner.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=myScanner.next();
	}while(answer.equals("Y") || answer.equals("y"));
}
  // Chen's method
  public static String listArray(int num[]){
	String out = "{";
	for(int j = 0; j < num.length; j++){
  	if(j > 0){
    	out += ", ";
  	}
  	out += num[j];
	}
	out += "} ";
	return out;
  }
}
