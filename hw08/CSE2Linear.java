/// CSE2Linear HW08
/// Marta Kasica-Soltan
/// April 6, 2018
/// CSE2S-210
/// Uses binary then linear search to search for the grade you want amoung inputs you gave

import java.util.Scanner;  // Import Scanner class
import java.util.Random;  // Import Random class
public class CSE2Linear {
  // Create a method to check if an input is an integer
  // No input, output integer
  public static int integerChecker(){
    Scanner myScanner = new Scanner(System.in);  // Call a scanner myScanner
    String useless = "";  // Use string for inputs you don't want
    // Continue running loop if there's not an int
    while (!myScanner.hasNextInt()){
        System.out.print("I'm sorry, that is not an integer. Try again: ");  // Error msg
        useless = myScanner.nextLine();  // Empty field to ask again
      }
    int test = myScanner.nextInt();  // Set variable to integer user input
    return test;  // Return this integer
  }
  // Create method to scramble an array
  public static int[] scrambler(int[] finals){
    Random myRandom = new Random();  // Create a random called myRandom
    int newFinals[] = new int[finals.length-1];  // Set new array and allocate 15
                                                 // Keep in mind original had 16 for neg start
    // Fill array with negative 1
    for (int c = 0; c < newFinals.length; c++){
      newFinals[c] = -1;
    }
    int count = 0;  // Initialize a counter for randomization
    int target = 0;  // Random position in new array to be switched
    int temp = 0;  // Value you're taking from original array
    // Use while loop to 
    while (count < 15){
      // Randomly choose target position in new array
      target = myRandom.nextInt(15);
      temp = finals[count+1];  // Add one because of the negative start
      if (newFinals[target] < 0){  // If the pos in array still is -1, means 
                                   // it hasn't been changed, so..
        newFinals[target] = temp;  // Target pos is given new value
        count++;  // Indicate a change in the new array
      }
    }
    return newFinals;  // Return new array
  }
  // Create method to print an array
  public static void returnArray(int [] array){
    // Print every aspect of the array
    for (int a = 0; a < array.length; a++){
      System.out.print(array[a]+" ");
    }
  }
  // Create method for searching through an array linearly
  public static void linearSearch(int [] array){
    Scanner myScanner = new Scanner(System.in);  // Create scanner myScanner
    System.out.println("Please enter a grade to search for: ");  // Ask for grade
    int enteredGrade = myScanner.nextInt();  // User input is enteredGrade
    int counter = 0;  // How many array ints you go by to find the match
    boolean foundIt = false;  // True if you find a match ^
    // Checks for value linearly
    for (int a = 0; a < array.length; a++){
      if (array[a] == enteredGrade){
        foundIt = true;  // Match found, true
        counter++;  // Increments to keep consistency
      }
      // If not found, print error message saying how many iterations it took regardless
      if (counter == array.length-1 && foundIt == false){
        // Add one to increment because if statement above has an increment this bypasses that
        System.out.println("The grade could not be found. Took "+(counter+1)+" iteration(s)");
        break;  // Break out of for loop
      }
      // If found, print out how many iterations it took
      else if (foundIt == true){
        System.out.println("The grade was found. Took "+counter+" iteration(s)");
        break;  // Break out of for loop
      }
      counter++;  // Increment every time
    }
  }
  // Create method to search binarily
  public static void binarySearch(int[] array){
    Scanner myScanner = new Scanner(System.in);  // Create new scanner myScanner
    System.out.println("Please enter a grade to search for: ");  // Prompt user for a grade
    int enteredGrade = myScanner.nextInt();  // Store grade as int
    int counter = 0;  // Use counter to keep track of place in array
    boolean foundIt = false;  // Only true if found in array
    int minIndex = 1;  // Ignore the -1, set the min of what you're searching
    int maxIndex = array.length - 1;  // Set the max of what you're searching
    int check = (int)((maxIndex + minIndex)/2);  // This will be the place to check in the array
    while (foundIt == false){
      if (array[check] == enteredGrade){
        counter++;  // Increment if you've found it
        foundIt = true;  // Set to true 
      }
      // If the number checked is too large, go lower
      else if (array[check] > enteredGrade){
        maxIndex = check-1;  // Set new max to match
        check = (maxIndex+minIndex)/2;  // Set new check value based on min/max
        counter++;  // Increment no matter what
      }
      // If the number checked is too small, go higher
      else if (array[check] < enteredGrade){
        minIndex = check+1;  // Change min to match
        check = (maxIndex+minIndex)/2;  // Set new check value based on min/max
        counter++;  // Increment no matter what
      }
      // If the values go too high or low, the entered grade isn't here
      if (check < 1 || check > 15){
        break;
      }
    }
    // Print that it's been found if it has
    if (foundIt == true){
      System.out.println("The grade was found. Took "+counter+" iteration(s)");
    }
    // Print that it hasn't been found if it hasn't
    if (foundIt == false){
      System.out.println("The grade could not be found. Took "+counter+" iteration(s)");
    }
  }
  // Main method
  public static void main (String [] args){
    Random myRandom = new Random();  // Create random myRandom
    Scanner myScanner = new Scanner(System.in);  // Create scanner myScanner
    String useless = "";  // String to store bad input
    int test = 0;  // Temporarily set to whatever value user inputs to check it
    int finals[] = new int[16];  // Set to 16 to include a negative start
    // Prompt user for values
    System.out.println("Please enter 15 integers. They must be 0-100, ascending.");
    finals[0] = -1;  // Set first value of array to -1 to be able to test for negativity
    for (int a = 1; a < 16; a++){
      test = integerChecker();  // Set test value using integer checking method
      // Continue this so long as the input doesn't match what is needed
      while (test > 100 || test < 0 || test < finals[a-1]){
        if (test > 100 || test < 0){
          // If input is out of range, print an out of range message
          System.out.println("I'm sorry, that is out of range. Try again: ");
        }
        else if (test < finals[a-1]){
          // If input is less than previous input, print error message
          System.out.println("I'm sorry, that is not ascending order. Try again: ");
        }
        // Test the integer again
        test = integerChecker();
      }
      finals[a] = test;  // After it passes all requirements, set it as the next value in array
    }
    // Print out array skipping the -1
    for (int b = 1; b < finals.length; b++){
      System.out.print(finals[b]+" ");
    }
    System.out.println();  // Print new line
    binarySearch(finals);  // Call binarySearch for finals array
    int newFinals[] = scrambler(finals);  // Create new array for scrambled finals array
    returnArray(newFinals);  // Print out this array by calling returnArray method
    System.out.println();  // Print new line
    linearSearch(newFinals);  // Call linearSearch for new array
    
  }
}