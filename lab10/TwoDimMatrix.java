///////////////////////////////
/// TwoDimMatrix LAB10
/// Marta Kasica-Soltan
/// April 20, 2018
/// CSE2S-210
/// 

public class TwoDimMatrix{
  public static int [][] increasingMatrix(int width, int height, boolean format){
    // If format is true, row major
    int [][] myArray = new int [height][width];
    if (format == true){
      int counter = 1;
      for (int a = 0; a < height; a++){
        for (int b = 0; b < width; b++){
          myArray[a][b] = counter;
          counter++;
        }
      }
    }
    // If format is false, column major
    else {
      int counter = 1;
      for (int a = 0; a < width; a++){
        for (int b = 0; b < height; b++){
          myArray[b][a] = counter;
          counter++;
        }
      }
    }
    return myArray;
  }
  public static void printMatrix(int[][] array, boolean format){
    if (array == null){
      System.out.println("The array was empty!");
    }
    // If format is true, row major
    else if (format == true){
      for (int a = 0; a < array.length; a++){
        for (int b = 0; b < array[0].length; b++){
          System.out.print(array[a][b]+" ");
        }
        System.out.println();
      }
    }
    else {
      for (int a = 0; a < array[0].length; a++){
        for (int b = 0; b < array.length; b++){
          System.out.print(array[b][a]+" ");
        }
        System.out.println();
      }
    }
  }
  public static int [][] translate(int[][] array){
    int [][] output = new int [array[0].length][array.length];
    for (int a = 0; a < array[0].length; a++){
      for (int b = 0; b < array.length; b++){
        output[a][b] = array[b][a];
      }
    }
    return output;
  }
  public static int [][] addMatrix(int[][] a, boolean formatA, int[][] b, boolean formatB){
    if (formatA == false){
      a = translate(a);
    }
    if (formatB == false){
      b = translate(b);
    }
    if (a.length == b.length && a[0].length == b[0].length){
      int [][] addition = new int [a.length][a[0].length];
      for (int i = 0; i < a.length; i++){
        for (int j = 0; j < a[0].length; j++){
          addition[i][j] = a[i][j] + b[i][j];
        }
      }
      return addition;
    }
    else {
      System.out.println("These arrays cannot be added!");
      return null;
    }
  }
  public static void main (String [] args){
    /*int [][] array = new int [3][2];
    array = increasingMatrix(2, 3, false);
    printMatrix(array, true);
    array = translate(array);
    printMatrix(array, false); */
    int width = ((int)(Math.random()*3.99 + 2));
    int height = ((int)(Math.random()*3.99 + 2));
    int width1 = ((int)(Math.random()*3.99 + 2));
    int height1 = ((int)(Math.random()*3.99 + 2));
    int [][] arrayA = new int [height][width];
    arrayA = increasingMatrix(width, height, true);
    int [][] arrayB = new int [height][width];
    arrayB = increasingMatrix(width, height, false);
    int [][] arrayC = new int [height1][width1];
    arrayC = increasingMatrix(width1, height1, true);
    System.out.println("Array A with width "+width+" and height "+height);
    printMatrix(arrayA, true);
    System.out.println("Array B with width "+height+" and height "+width);
    printMatrix(arrayB, false);
    System.out.println("Array C with width "+width1+" and height "+height1);
    printMatrix(arrayC, true);
    int [][] sumAB = new int [height][width];
    int [][] sumAC = new int [height][width];
    sumAB = addMatrix(arrayA, true, arrayB, false);
    sumAC = addMatrix(arrayA, true, arrayC, true);
    System.out.println("Sum of A and B:");
    printMatrix(sumAB, true);
    System.out.println("Sum of A and C:");
    printMatrix(sumAC, true);
  }
}