///////////////////////////////
/// Arithmetic HW02
/// Marta Kasica-Soltan
/// February 3, 2018
/// CSE2S-210
/// Calculates the total price with and without tax for pants, shirts, and belts

public class Arithmetic {
  // Main method for every Java program
  public static void main(String[] args) {
    
    // Input data
    int numPants = 3,  // Number of pairs of pants
    numShirts = 2,  // Number of sweatshirts
    numBelts = 1;  // Number of belts
    double pantsPrice = 34.98,  // Cost per pair of pants
    shirtPrice = 24.99,  // Cost per sweatshirt
    beltPrice = 33.99,  // Cost per belt
    paSalesTax = 0.06;  // The tax rate
    
    // Declaring total and tax variables
    double totalCostPants, totalCostShirts, totalCostBelts,  // Total cost of respective clothing item with tax
    salesTaxPants, salesTaxShirts, salesTaxBelts, totalSalesTax,  // Sales tax on each respective item, and total sales tax
    costPants, costShirts, costBelts,  // Cost of each respective item without tax  
    totalCost, totalCostPlusSales;  // First is total cost without sales tax, second is with
    
    // Calculations
    costPants = numPants*pantsPrice;  // Determining cost of pants without tax
    costShirts = numShirts*shirtPrice;  // Determining cost of shirts without tax
    costBelts = numBelts*beltPrice;  // Determining cost of belts without tax
    
    salesTaxPants = numPants*pantsPrice*paSalesTax;  // Sales tax on pants
    salesTaxShirts = numShirts*shirtPrice*paSalesTax;  // Sales tax on shirts
    salesTaxBelts = numBelts*beltPrice*paSalesTax;  // Sales tax on belts
    
    // Printing the cost without tax and the sales tax for all clothing items
    System.out.println("Pants cost $"+(costPants)+
        " without tax, and their tax is $"+((Math.round(salesTaxPants*100.0))/100.0));
    System.out.println("Shirts cost $"+(costShirts)+
        " without tax, and their tax is $"+((Math.round(salesTaxShirts*100.0))/100.0));
    System.out.println("Belts cost $"+(costBelts)+
        " without tax, and their tax is $"+((Math.round(salesTaxBelts*100.0))/100.0));
    
    totalCost = costPants+costShirts+costBelts;  // Total cost without tax
    totalSalesTax = salesTaxPants+salesTaxShirts+salesTaxBelts;  // Just the total sales tax
    
    totalCostPants = costPants+salesTaxPants;  // Actual cost of pants, with tax
    totalCostShirts = costShirts+salesTaxShirts;  // Actual cost of shirts, with tax
    totalCostBelts = costBelts+salesTaxBelts;  // Actual cost of belts, with tax
    totalCostPlusSales = totalCostPants+totalCostShirts+totalCostBelts;  // Final bill, all clothes and tax
    
    // Printing the cost without the sales tax
    System.out.println("The total cost of all three purchases is $"
        +(totalCost)+" without tax");
    // Printing the sales tax
    System.out.println("The total sales tax is $"+((Math.round(totalSalesTax*100.0))/100.0));
    // Printing the total bill
    System.out.println("The total bill, including sales tax, is $"+((Math.round(totalCostPlusSales*100.0))/100.0));
    
  }
}