 ///////////////////////////////
/// Twist Generator LAB05
/// Marta Kasica-Soltan
/// March 2, 2018
/// CSE2S-210
/// Prints an encrypted X amoung stars

import java.util.Scanner;  // Imports scanner class
public class encrypted_x {
  // Main method for every Java program
  public static void main (String [] args){
    // Calls and names new scanner
    Scanner myScanner = new Scanner(System.in);
    int length = 0;  // Desired length of twist
    int test = 0;  // Variable for assuring input 1-100
    
    // Use do-while to check that the input is an integer by 
    // repetitively prompting the user for an integer until
    // they input one, then test for 1-100 of integer
    do {
      System.out.println("Please input an integer 1-100: ");
      while (!myScanner.hasNextInt()) {  // If input isn't integer, repeat loop
        String junkWord = myScanner.next();  // Clear scanner
        System.out.println("Error. Please input an integer 1-100: ");  // Prompt
      }
      test = myScanner.nextInt();  // Assign input as test
    } while (test < 1 || test > 100);  // Will repeat if not btwn 1-100
    length = test;  // Sets length as test, given it's passed all criteria
    // Printing the X
    for (int i = 0; i < length; i++){  // Loops for each line
      for (int j = 0; j < length; j++){  // Loops for each space in a line
        // If the line number matches the space number, or it matches the 
        // length minus the line number plus 1, print the X
        if (i == j || j == length - (i+1)){
          System.out.print(" ");
        }
        // Otherwise, print the encryption
        else {
          System.out.print("*");
        }
      }
      // Set a new line in the string every time the first for loop goes
      System.out.print("\n");
    }
  }
}
