 ///////////////////////////////
/// Twist Generator LAB05
/// Marta Kasica-Soltan
/// March 2, 2018
/// CSE2S-210
/// Prints an encrypted X amoung stars

import java.util.Scanner;  // Imports scanner class
public class encrypted_x_dowhile {
  // Main method for every Java program
  public static void main (String [] args){
    // Calls and names new scanner
    Scanner myScanner = new Scanner(System.in);
    int length = 0;  // Desired length of twist
    int test = 0;  // Variable for assuring input 1-100
    
    // Use do-while to check that the input is an integer by 
    // repetitively prompting the user for an integer until
    // they input one, then test for 1-100 of integer
    do {
      System.out.println("Please input an integer 1-100: ");
      while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.println("Error. Please input an integer 1-100: ");
      }
      test = myScanner.nextInt();
    } while (test < 1 || test > 100);  // Will repeat if not btwn 1-100
    length = test;
    int i = 0, j;
    do{
      j = 0;
      do{
        if (i == j || j == length - (i+1)){
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
        j++;
      }while (j < length);
      System.out.println();
      i++;
    }while (i < length);
  }
}
