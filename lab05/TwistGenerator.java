///////////////////////////////
/// Twist Generator LAB05
/// Marta Kasica-Soltan
/// March 2, 2018
/// CSE2S-210
/// Prints out a twist
  
import java.util.Scanner;  // Imports scanner class
public class TwistGenerator {
  // Main method for every Java program
  public static void main (String [] args){
    // Calls and names new scanner
    Scanner myScanner = new Scanner(System.in);
    int length = 0;  // Desired length of twist
    int test = 0;  // Variable for assuring positivity of input
    
    // Use do-while to check that the input is an integer by 
    // repetitively prompting the user for an integer until
    // they input one, then test for positivity of integer
    do {
      System.out.println("Please input a positive integer: ");
      while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.println("Please input a positive integer: ");
      }
      test = myScanner.nextInt();
    } while (test <= 0);  // If integer negative, repeats do loop
    
    length = test;  // Since input is + int, it is the length
    // Initialilze a string by setting its value to the first piece
    // of the string. This will always be the beginning of it.
    String myTwist1 = "\\";
    int i;  // Used to keep loop going until this reaches length's value
    // Continue through loop for each value i to length, incrementing
    // by one. For each value, use switch to add to the string.
    for (i = 2; i<=length; i++){
      int a = i%3;  // Will result in 0-2, always
      switch (a){
        case 0: myTwist1 = myTwist1 + "/";  // Adds third piece of twist
          break;                            // to the string
        case 1: myTwist1 = myTwist1 + "\\";  // Adds first piece
          break;
        case 2: myTwist1 = myTwist1 + " ";  // Adds second piece
          break;
        default: myTwist1 = myTwist1;  // String stays same
          break;
      }
    }
    // Initialize a string, same as above, for second line of twist
    String myTwist2 = " ";
    // Use for loop and switch statement as above, but different case
    // outputs due to the different visual of the second line
    for (i = 2; i<=length; i++){
      int b = i%3;
      switch (b){
        case 0: myTwist2 += " ";
          break;
        case 1: myTwist2 += " ";
          break;
        case 2: myTwist2 += "X";
          break;
        default: myTwist2 = myTwist2;
          break;
      }
    }
    // Initialize a string, same as above, for third line of twist
    String myTwist3 = "/";
    // Use loop and switch statement as above, but different case
    // outputs due to the different visual of the third line
    for (i = 2; i<=length; i++){
      int a = i%3;
      switch (a){
        case 0: myTwist3 += "\\"; 
          break;
        case 1: myTwist3 += "/";
          break;
        case 2: myTwist3 += " ";
          break;
        default: myTwist3 = myTwist3;
          break;
      }
    }
    // Print out the twist, one line at a time, returning after lines
    System.out.println(myTwist1);
    System.out.println(myTwist2);
    System.out.print(myTwist3);
    
  }
}