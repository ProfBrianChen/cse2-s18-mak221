/// Convert HW03
/// Marta Kasica-Soltan
/// February 9, 2018
/// CSE2S-210
/// Tells cubic miles of rainfall on selected acres

import java.util.Scanner;  // Import scanner class

public class Convert {
  // Main method for every Java program
  public static void main(String[] args) {
    // Initialize method for obtaining inputs
    Scanner myScanner = new Scanner(System.in);
    // Ask for acres of affected land
    System.out.print("Enter the acres of land affected: ");
    double acres = myScanner.nextDouble();  // Store acres as a double
    // Ask for amount of rainfall
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfallInches = myScanner.nextDouble();  // Store inches of rain
    double squareMiles,  // Square miles of affected land
    cubicMiles,  // Cubic miles of affected land
    rainfallMiles;  // Rainfall converted to miles
    squareMiles = acres*0.0015625;  // Converting acres to sq mi
    rainfallMiles = rainfallInches/12/5280;  // Converting rainfall inches to miles
    cubicMiles = rainfallMiles*squareMiles;  // Calculating rain in cubic miles
    // Print out final calculation of cubic miles of rain
    System.out.println((cubicMiles)+" cubic miles"); 
  }
}