/// Pyramid HW03
/// Marta Kasica-Soltan
/// February 9, 2018
/// CSE2S-210
/// Gives the volume of a pyramid based on dimensions

import java.util.Scanner;  // Import scanner class

public class Pyramid {
  // Main method required for every Java program
  public static void main (String[] args) {
    // Initialize scanner for obtaining inputs
    Scanner myScanner = new Scanner(System.in);
    // Ask for the square side length of pyramid
    System.out.print("The square side of the pyramid has the length of: ");
    // Store value for side length
    double squareSideLength = myScanner.nextDouble();
    // Ask for the height of pyramid
    System.out.print("The height of the pyramid has the length of: ");
    // Store value for height length
    double heightLength = myScanner.nextDouble();
    double volume;  // Final volume of pyramid
    // Calculate volume using volume formula
    volume = Math.pow(squareSideLength, 2)*heightLength/3;
    // Print out final volume value
    System.out.print("The volume inside the pyramid is: "+(volume));
  }
}