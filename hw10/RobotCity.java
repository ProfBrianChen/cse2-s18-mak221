/// RobotCity HW10
/// Marta Kasica-Soltan
/// April 23, 2018
/// CSE2S-210
/// Builds a randomly generated city and invades it with robots

public class RobotCity {
  // Method to build the random city
  // No input, 2D array output
  public static int [][] buildCity(){
    // Set random integers for height and width, 10-15
    int width = ((int)((Math.random()*5.99)+10));
    int height = ((int)((Math.random()*5.99)+10));
    // Allocate space for new array using said random values
    int [][] cityArray = new int[height][width];
    // Two for loops for adding values to each "city block"
    for (int a = 0; a < height; a++){
      for (int b = 0; b < width; b++){
        // Set each value in the array to a random number 100-999
        cityArray[a][b] = ((int)((Math.random()*899.99)+100));
      }
    }
    return cityArray;  // Return the array generated
  }
  // Method to display contents of the city
  // 2D array input, no output
  public static void display(int [][] city){
    // Set a nested for loop up for printing the city
    for (int a = 0; a < city.length; a++){
      for (int b = 0; b < city[0].length; b++){
        // Print out each value in a grid format, allocating 5 spaces per int
        System.out.printf("% 5d", city[a][b]);
      }
      System.out.println();  // Skip a line after each outer loop
    }
  }
  // Method for robots invading the blocks in the city
  // 2D array and int input, 2D array output
  public static int [][] invade(int [][] city, int k){
    // Use for loop to invade city with robots
    for (int a = 0; a < k; a++){
      // Randomly assign coordinates (width and height) to each robot
      int width = ((int)((Math.random()*city[0].length)));
      int height = ((int)((Math.random()*city.length)));
      // If there's already a robot there, noted by a negative,
      // decrement a so that the loop will go again, making sure
      // each robot gets a block
      if (city[height][width] < 0){
        a--;
        continue;
      }
      // Otherwise, let the robot inhabit the city by making it negative
      else{
        city[height][width] = -city[height][width];
      }
    }
    return city;  // Return the new array with robots
  }
  // Method for moving robots along the array
  // 2D array as input, 2D array output
  public static int [][] update(int [][] city){
    // Test every block of the city, east to west, south to north
    // to avoid checking robots that were moved east
    for (int a = city.length-1; a > -1; a--){
      for (int b = city[0].length-1; b > -1; b--){
        // If the robot isn't on the edge and it exists (bc negative)
        // remove robot from current space and move it one block to the right
        if (city[a][b] < 0 && b != city[0].length-1){
          city[a][b] = -city[a][b];
          // Check to make sure a robot isn't already there
          // so that if it is, you don't remove it
          if (city[a][b+1] > 0){
            city[a][b+1] = -city[a][b+1];
          }
        }
        // If the robot is on the edge, just remove the robot
        if (b == city[0].length-1 && city[a][b] < 0){
          city[a][b] = -city[a][b];
        }
      }
    }
    return city;  // Return updated city with moved bots
  }
  // Main method
  public static void main (String [] args){
    int [][] cityArray = buildCity();  // Declare array for city and build it
    display(cityArray);  // Display the randomly built city
    int k = ((int)((Math.random()*99.99)+1));  // Randomly create k 1-100 for # of bots
    cityArray = invade(cityArray, k);  // Invade city with random number of bots
    System.out.println();  // New line
    display(cityArray);  // Display invaded city
    // Update invaded city five times and display it each time
    for (int a = 0; a < 5; a++){
      cityArray = update(cityArray);
      System.out.println();  // New line
      display(cityArray);
    }
  }
}