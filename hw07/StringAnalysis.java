/// Area HW07
/// Marta Kasica-Soltan
/// March 25, 2018
/// CSE2S-210
/// Checks if the string input is completely letters. Returns false if not.

import java.util.Scanner;  // Import scanner class
// Create class
public class StringAnalysis {
  // Create method for determining whether input is letter for entire string
  public static boolean stringAnswer(String input){
    int length = input.length() - 1;  // (DIF) Length of string is one more than how many spaces it has
    // For ex, a string y-e-s is 3, but s is in the 2nd space.
    // Create for loop to go through each character
    for (int a = 0; a <= length; a++){
      // If not between or equal to a and z, it is not a letter
      if ('a' > input.charAt(a) || input.charAt(a) > 'z'){
        return false;  // Leave method, return false
      }
    }
    return true;  // Otherwise, return true
  }
  // Create method for only checking a certain number of spaces
  public static boolean stringAnswer(String input, int spaces){
    // If user asks for more spaces than exist, change value to total number of spaces
    if (spaces > input.length()){
      spaces = input.length();
    }
    int length = spaces - 1;  // Compensate for difference stated above (DIF)
    // Make a for loop to check each space
    for (int a = 0; a <= length; a++){
      // If not between a and z, it isn't a character
      if ('a' > input.charAt(a) || input.charAt(a) > 'z'){
        return false;  // Return false
      }
    }
    return true;  // Otherwise return true
  }
  // Main method
  public static void main (String [] args) {
    Scanner myScanner = new Scanner(System.in);  // Call object scanner for new scanner myScanner
    System.out.print("Please input a string:  ");  // Ask for a string
    String input = myScanner.next();  // Store string as input
    int test = 0;  // Dummy variable
    // Ask user how many characters they want checked
    System.out.print("Would you like the entire string to be checked? 0 for yes. If no, how many characters do you want checked? :  ");
    // Ensure the input is an integer and pos
    do {
      while (!myScanner.hasNextInt()) {  // Given the input is not an integer...
        String junkWord = myScanner.next();  // Clear scanner input to allow for another
        // Print an error, ask again for the input
        System.out.print("Error. Please enter an int. 0 for yes, any other positive integer for the amount you want checked:  ");
      }
      test = myScanner.nextInt();  // File input as test variable
      if (test < 0) {
        // Print error message with followup
        System.out.print("Error. Please enter an int. 0 for yes, any other positive integer for the amount you want checked:  :  ");
      }
    } while (test < 0);  // If it's negative, try again
    boolean finalAnswer = true;  // Initialize the answer you will give to the user
    // If the user wants all characters checked
    if (test == 0){
      finalAnswer = stringAnswer(input);  // Call loop that checks if they're all letters, but with no int input
    }
    // If the user wants select number of characters checked
    if (test > 0){
      int spaces = test;  // Set value they want checked to spaces
      finalAnswer = stringAnswer(input, spaces);  // Call loop that checks if certain number are letters
    }
    System.out.println(finalAnswer);  // Print answer
  }
}