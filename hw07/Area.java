/// Area HW07
/// Marta Kasica-Soltan
/// March 25, 2018
/// CSE2S-210
/// Takes user input and gives desired area of choice (either rect, triangle, or circle)

import java.util.Scanner;  // Import scanner class
public class Area {
  public static double inputCheck(){  // Create method for checking input validity (doubles)
    double test = 0;  // Dummy variable used for testing
    Scanner myScanner = new Scanner(System.in);  // Name scanner object myScanner
    // Create loop to check for double and positivity
    do {
      while (!myScanner.hasNextDouble()) {  // Given the input is not a double...
        String junkWord = myScanner.next();  // Clear scanner input to allow for another
        // Print an error, ask again for the input
        System.out.print("Error. Please enter a positive double:  ");
      }
      test = myScanner.nextDouble();  // File input as test variable
      if (test < 0) {  // If negative,
        // Print out another error message if it's negative
        System.out.print("Error. Please enter a positive double:  ");
      }
    } while (test <= 0);  // Run while negative
  return test;  // Return the initial value checked for if it passes
  }
  // Create method to find the area of a rectangle
  public static double rectangleArea(double width, double length){
    return width*length;  // Multiply the width and length and return the value
  } 
  // Create method to find the area of a triangle
  public static double triangleArea(double base, double height){
    return (0.5*base*height);  // Area is 1/2*b*h, return the value
  }
  // Create method to find the area of a circle
  public static double circleArea(double radius){
    return Math.pow(radius, 2)*3.14159;  // Area of circle is pi*r^2. Return value
  }
  // Main method
  public static void main (String [] args){
    Scanner myScanner = new Scanner(System.in);  // Call a scanner object, name it myScanner
    // Ask user which shape they want to find the area of
    System.out.print("Which shape would you like the area of? Three choices: rectangle, triangle, or circle:  ");
    String test = myScanner.nextLine();  // Use test variable for validity purposes, dummy variable
    // If input doesnt match, error
    while (!test.equals("rectangle") && !test.equals("triangle") && !test.equals("circle")){
      System.out.print("Error. You can only choose rectangle, triangle, or circle:  ");
      test = myScanner.nextLine();  // After asking again, set dummy variable again
    }
    String shapeInput = test;  // Once input matches, continue and use shapeInput as their input
    // If they want rectangle area
    if (shapeInput.equals("rectangle")){
      // Ask for length
      System.out.print("Please enter a length:  ");
      double length = inputCheck();  // Store as length of rectangle
      // Ask for width
      System.out.print("Please enter a width:  ");
      double width = inputCheck();  // Store as width of rectangle
      double rectangleArea = rectangleArea(width, length);  // Call method to find area of rect
      System.out.println(rectangleArea);  // Print area
    }
    // If they want triangle area
    else if (shapeInput.equals("triangle")){
      // Ask for height
      System.out.print("Please enter a height:  ");
      double height = inputCheck();  // Store as height of triangle
      // Ask for base
      System.out.print("Please enter a base:  "); 
      double base = inputCheck();  // Store as base of triangle
      double triangleArea = triangleArea(base, height);  // Call method to find area of triangle
      System.out.println(triangleArea);  // Print area
    }
    // If they want circle area
    else if (shapeInput.equals("circle")){
      // Ask for radius
      System.out.print("Please enter a radius:  ");
      double radius = inputCheck();  // Store as radius of circle
      double circleArea = circleArea(radius);  // Call method to find area of circle
      System.out.println(circleArea);  // Print area
    }
  }
}