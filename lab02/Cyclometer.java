///////////////////////////////
/// Cyclometer LAB02
/// Marta Kasica-Soltan
/// February 2, 2018
/// CSE2S-210
/// Measures time, counts, and distance for different trips
public class Cyclometer {
  // Main method required for every Java program
  public static void main(String[] args) {
    
    // Input data
    int secsTrip1=480;  // Seconds for first trip
    int secsTrip2=3220;  // Seconds for second trip
    int countsTrip1=1561;  // Counts for first trip
    int countsTrip2=9037;  // Counts for the second trip
    
    // Intermediate variables and output data
    double wheelDiameter=27.0;  // Diameter of bike wheel
    double PI=3.14159;  // Rounded numerical pi value
    double feetPerMile=5280;  // How many feet are in a mile
    double inchesPerFoot=12;  // How many inches in a foot
    double secondsPerMinute=60;  // How many seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance;  // Creates variables to track distance outputs
    
    // Printing out numbers for minutes and counts for both trips
    System.out.println("Trip 1 took "+
        (secsTrip1/secondsPerMinute)+" minutes and had "+
        countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
        (secsTrip2/secondsPerMinute)+" minutes and had "+
        countsTrip2+" counts.");
    // Calculations
  distanceTrip1=countsTrip1*wheelDiameter*PI;  // Gives distance in inches
  distanceTrip1=distanceTrip1/inchesPerFoot/feetPerMile;  // Gives distance in miles
  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;  // Gives distance for trip 2 in miles
  totalDistance=distanceTrip1+distanceTrip2; // Calculates total distance
  
  // Printing out output data
  System.out.println("Trip 1 was "+distanceTrip1+" miles");  // Prints out miles of first trip
  System.out.println("Trip 2 was "+distanceTrip2+" miles");  // Prints out miles of second trip
  System.out.println("The total distance was "+totalDistance+" miles");  // Prints out total miles
  }
}
