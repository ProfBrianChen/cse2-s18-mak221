/// Hw06 HW06
/// Marta Kasica-Soltan
/// March 3, 2018
/// CSE2S-210
/// Prompts user for their course information

import java.util.Scanner;  // Import scanner class
public class Hw05 {
  public static void main(String[] args) {
    String junkWord = "null";  // Initialize string for clearing scanner
    // Initialize scanner for incoming inputs
    Scanner myScanner = new Scanner(System.in);
    // Ask user for course number
    System.out.println("Please input your course number: ");
    // Use while loop to ensure the input is an integer
    // If it isn't one, continue asking until there is one
    while (!myScanner.hasNextInt()) {  
      junkWord = myScanner.next();  // Throw away the non-integer input
      System.out.println("That was not an integer. Please input your course number: ");
    }
    int courseNumber = myScanner.nextInt();  // Store given value as the course number
    // Ask user for the department name
    System.out.println("Please input your department name: ");
    // Use while loop to ensure the input is a string
    // To ensure the user inputs words and not numbers, check for existence of an int
    // If integers exist, ask for a new input
    while (myScanner.hasNextInt()) {  
      junkWord = myScanner.next();
      System.out.println("That was not a string. Please input your department name: ");
    }
    String departmentName = myScanner.next();  // Store given string as department name
    // Ask how many times it meets in a week
    System.out.println("Please input how many times the class meets in a week: ");
    // Use while loop to ensure the input is an integer, see above while loop for int
    while (!myScanner.hasNextInt()) {  
      junkWord = myScanner.next();
      System.out.println("That was not an integer. Please input how many times the class meets in a week: ");
    }
    int timePerWeek = myScanner.nextInt();  // Store value as the number of times per week
    // Ask when the class starts
    System.out.println("Please input the time the class begins, in military time, without a colon: ");
    // Use while loop to ensure the input is an integer, see above while loop for int
    while (!myScanner.hasNextInt()) {  
      junkWord = myScanner.next();
      System.out.println("That was not an integer. Please input the time the class begins: ");
    }
    int classTime = myScanner.nextInt();  // Store value as the class time
    // Ask user for their instructor's name
    System.out.println("Please input your instructor's name: ");
    // Use while loop to ensure the input is a string, see above while loop for string
    while (myScanner.hasNextInt()) {  
      junkWord = myScanner.next();
      System.out.println("That was not a string. Please input your instructor's name: ");
    }
    String instructorName = myScanner.next();  // Store given string as instructor's name
    // Ask how students are in the class
    System.out.println("Please input how many students are in the class: ");
    // Use while loop to ensure the input is an integer, see above while loop for int
    while (!myScanner.hasNextInt()) {  
      junkWord = myScanner.next();
      System.out.println("That was not an integer. Please input how many students are in the class: ");
    }
    int students = myScanner.nextInt();  // Store value as the number of students in the class
  }
}