///////////////////////////////
/// Arrays LAB08
/// Marta Kasica-Soltan
/// April 13, 2018
/// CSE2S-210
/// Teaches about call by value using arrays

public class Arrays {
  public static int [] copy (int [] array){
    int newArray[] = new int[array.length];
    for (int a = 0; a < array.length; a++){
      newArray[a] = array[a];
    }
    return newArray;
  }
  public static void inverter (int [] array) {
    for (int a = 0; a < array.length/2; a++){
      int temp = array[a];
      array[a] = array[array.length-1-a];
      array[array.length-1-a] = temp;
    }
  }
  public static int [] inverter2 (int [] array){
    int newArray[] = new int[array.length];
    newArray = copy(array);
    inverter(newArray);
    return newArray;
  }
  public static void print (int [] array){
    for (int a = 0; a < array.length; a++){
      System.out.print(array[a] + " ");
    }
  }
  public static void main (String [] args){
    int array[] = {2, 3, 4, 5, 6, 7, 8, 9};
    int array0[] = copy(array);
    int array1[] = copy(array);
    int array2[] = copy(array);
    inverter(array0);
    print(array0);
    System.out.println();
    inverter2(array1);
    print(array1);
    System.out.println();
    int array3[] = inverter2(array2);
    print(array3);
  }
}