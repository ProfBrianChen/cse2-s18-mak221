///////////////////////////////
/// Check LAB03
/// Marta Kasica-Soltan
/// February 9, 2018
/// CSE2S-210
/// Splitting the check and tip evenly between people

import java.util.Scanner;  // Import scanner class

public class Check{
  // Main method required for every Java program
  public static void main(String[] args) {
   // Initialize the method for obtaining inputs
   Scanner myScanner = new Scanner(System.in);
    // Ask for original cost of check
    System.out.println("Enter the original cost of the check in the form xx.xx: ");
  double checkCost = myScanner.nextDouble();  // Store value for check
    // Ask for percentage of tip
    System.out.println("Enter the percentage tip that you wish to pay as a whole number in the form xx: ");
  double tipPercent = myScanner.nextDouble();  // Store tip % as whole #
  tipPercent /= 100; // Convert percentage into decimal
    // Ask for number of people to split check with
    System.out.println("Enter the number of people who went out to dinner: ");
  int numPeople = myScanner.nextInt();  // Store # value of people
  double totalCost;  // Total cost of dinner and tip
  double costPerPerson;  // Total cost split among people
  int dollars,   // Whole dollar amount of cost 
  dimes, pennies; // For storing digits
                  // to the right of the decimal point 
                  // for the cost
  totalCost = checkCost * (1 + tipPercent); // Calculate total cost with tip
  costPerPerson = totalCost / numPeople;  // Calculate cost per person
  // Get the whole amount, dropping decimal fraction
  dollars=(int)costPerPerson;
  //get dimes amount, e.g., 
  // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
  //  where the % (mod) operator returns the remainder
  //  after the division:   583%100 -> 83, 27%5 -> 2 
  dimes=(int)(costPerPerson * 10) % 10;
  pennies=(int)(costPerPerson * 100) % 100;
  // Print out final values for each person's bill
    System.out.println("Each person in the group owes $"+(dollars)+
  "."+(dimes)+(pennies));
  }  // End of main method   
} // End of class

