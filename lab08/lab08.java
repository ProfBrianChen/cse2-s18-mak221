///////////////////////////////
/// Lab08 LAB08
/// Marta Kasica-Soltan
/// April 6, 2018
/// CSE2S-210
/// Gives randomly generated grades for people

import java.util.Scanner;  // Importing the scanner class
public class lab08 {
  public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in);  // Declare scanner myScanner
    int randomNumber = (int)(Math.floor(Math.random() * 5) + 4);  // Generate random number 5-10
    String students[] = new String[randomNumber+1];  // Declaring and allocating an array named students that has 5-10 strings
    int midterm[] = new int[randomNumber+1];  // Declaring and allocating an array named midterm with same space^
    System.out.println("Please enter "+(randomNumber+1)+" student names: ");  // Prompt user for names
    //  Ask user for names for the students
    for (int a = 0; a < students.length; a++){
      System.out.print("");
      students[a] = myScanner.nextLine();
    }
    // Input random grades for people in midterm array
    for (int b = 0; b < students.length; b++){
      midterm[b] = (int)(Math.floor(Math.random() * 100));  
    }
    // Print out the midterm grades for each student as specified
    System.out.println("Here are the midterm grades of the "+(1+randomNumber)+" students above: ");
    for (int c = 0; c < students.length; c++){
      System.out.println(students[c] + ": " + midterm[c]);
    }
  }
}       