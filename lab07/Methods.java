///////////////////////////////
/// Methods LAB07
/// Marta Kasica-Soltan
/// March 25, 2018
/// CSE2S-210
/// Gives randomly generated sentences that form a story

import java.util.Random;  // Importing the random number class
import java.util.Scanner;  // Importing the scanner class
public class Methods {
 // Random randomGenerator = new Random();
  // Create a method to generate a random adjective
  public static String randomAdjective (){
    Random randomGenerator = new Random();
    String myWord = "";  // The word to be returned
    int randomInt = randomGenerator.nextInt(10);  // Generates a random # < 10
    // Use switch to attach a random word to the random number
    switch (randomInt) {
      case 0: myWord += "large";  // If randomInt is 0, this is the word
        break;  // Leave switch statement
      case 1: myWord += "furry";
        break;
      case 2: myWord += "spicy";
        break;
      case 3: myWord += "chubby";
        break;
      case 4: myWord += "boiling";
        break;
      case 5: myWord += "stern";
        break;
      case 6: myWord += "apathetic";
        break;
      case 7: myWord += "thirsty";
        break;
      case 8: myWord += "wiggly";
        break;
      case 9: myWord += "ripe";
        break;
      default: myWord += "";
        break;
    }
    return myWord;  // Have the method return randomly generated word
  }
  // Create a method to generate a random subject noun (non-primary) 
  public static String randomSubjectNoun (){
    Random randomGenerator = new Random();
    String myWord = "";  // The word to be returned
    int randomInt = randomGenerator.nextInt(10);  // Generates a random # < 10
    // Use switch to attach a random word to the random number
    switch (randomInt) {
      case 0: myWord += "squirrel";  // If randomInt is 0, this is the word
        break;  // Leave switch statement
      case 1: myWord += "grapefruit";
        break;
      case 2: myWord += "whale";
        break;
      case 3: myWord += "professor";
        break;
      case 4: myWord += "zucchini";
        break;
      case 5: myWord += "opossum";
        break;
      case 6: myWord += "toaster";
        break;
      case 7: myWord += "tablecloth";
        break;
      case 8: myWord += "grandmother";
        break;
      case 9: myWord += "yeast";
        break;
      default: myWord += "";
        break;
    }
    return myWord;  // Have the method return randomly generated word
  }
  // Create a method to generate a random past-tense verb
  public static String randomPastTenseVerb (){
    Random randomGenerator = new Random();
    String myWord = "";  // The word to be returned
    int randomInt = randomGenerator.nextInt(10);  // Generates a random # < 10
    // Use switch to attach a random word to the random number
    switch (randomInt) {
      case 0: myWord += "ate";  // If randomInt is 0, this is the word
        break;  // Leave switch statement
      case 1: myWord += "ransacked";
        break;
      case 2: myWord += "caressed";
        break;
      case 3: myWord += "opened";
        break;
      case 4: myWord += "ridiculed";
        break;
      case 5: myWord += "bathed";
        break;
      case 6: myWord += "purchased";
        break;
      case 7: myWord += "murdered";
        break;
      case 8: myWord += "drank";
        break;
      case 9: myWord += "sauteed";
        break;
      default: myWord += "";
        break;
    }
    return myWord;  // Have the method return randomly generated word
  }
  // Create a method to generate a random object noun (non-primary) 
  public static String randomObjectNoun (){
    Random randomGenerator = new Random();
    String myWord = "";  // The word to be returned
    int randomInt = randomGenerator.nextInt(10);  // Generates a random # < 10
    // Use switch to attach a random word to the random number
    switch (randomInt) {
      case 0: myWord += "knapsack";  // If randomInt is 0, this is the word
        break;  // Leave switch statement
      case 1: myWord += "olive";
        break;
      case 2: myWord += "grape";
        break;
      case 3: myWord += "mongoose";
        break;
      case 4: myWord += "toddler";
        break;
      case 5: myWord += "tortoise";
        break;
      case 6: myWord += "tree";
        break;
      case 7: myWord += "waffle";
        break;
      case 8: myWord += "mother";
        break;
      case 9: myWord += "snake";
        break;
      default: myWord += "";
        break;
    }
    return myWord;  // Have the method return randomly generated word
  }
 /* public static void main (String [] args){
    Scanner myScanner = new Scanner(System.in);
    boolean isGameDone = false;  // Sees if user wants another sentence
    String mySentence = "";  // Randomly generated sentence
    while (!isGameDone){
      mySentence = "";
      mySentence += "The ";
      mySentence += randomAdjective() + " ";
      mySentence += randomAdjective() + " ";
      mySentence += randomSubjectNoun() + " ";
      mySentence += randomPastTenseVerb() + " ";
      mySentence += "the ";
      mySentence += randomAdjective() + " ";
      mySentence += randomObjectNoun() + ".";
      System.out.println(mySentence);
      System.out.println("Would you like another sentence? 0 for no, any other integer for yes: ");
      int answer = myScanner.nextInt();
      if (answer == 0){
        isGameDone = true;
      }
      else{
        continue;
      }
    }
  }
  */
  // Create method for generating the thesis sentence
  public static String thesisSentence(){
    String mySentence = "";  // Randomly generated sentence
    String mySubject = "";  // Subject of sentence
    // Add rest of randomly generated sentence by calling methods for adjective, noun, etc.
    mySentence += "The ";
    mySentence += randomAdjective() + " ";
    mySentence += randomAdjective() + " ";
    mySubject = randomSubjectNoun();  // Set the subject
    mySentence += mySubject + " ";
    mySentence += randomPastTenseVerb() + " ";
    mySentence += "the ";
    mySentence += randomAdjective() + " ";
    mySentence += randomObjectNoun() + ".";
    System.out.println(mySentence);  // Print final sentence
    return mySubject;
    }
  // Create method that makes randomly generated supporting sentence
  public static void thesisSupport(String mySubject){
    String mySentence = "";  // Randomly generated sentence
    Random randomGenerator = new Random();  // Call object and create randomGenerator Random
    int randomInt = randomGenerator.nextInt(2);  // Randomly decide between using the subject and "it"
    // If gives 0 choose the subject
    if (randomInt == 0){
      mySentence += "This " + mySubject + " ";
    }
    // If gives 1 choose It
    if (randomInt == 1){
      mySentence += "It ";
    }
    // Add rest of randomly generated sentence by calling methods for adjective, noun, etc.
    mySentence += "was ";
    mySentence += randomAdjective() + " and ";
    mySentence += randomAdjective() + " ";
    mySentence += "while she ";
    mySentence += randomPastTenseVerb() + " the ";
    mySentence += randomAdjective() + " ";
    mySentence += randomObjectNoun() + ".";
    System.out.println(mySentence);  // Print final sentence
  }
  // Create method that makes randomly generated conclusion sentence
  public static void conclusionSentence(String mySubject){
    String mySentence = "";  // Randomly generated sentence
    // Add rest of randomly generated sentence by calling methods for adjective, noun, etc.
    mySentence += "That ";
    mySentence += randomAdjective() + " ";
    mySentence += mySubject + " ";
    mySentence += randomPastTenseVerb() + " ";
    mySentence += "her ";
    mySentence += randomObjectNoun() + "!";
    System.out.println(mySentence);  // Print final sentence
  }
  // Main method
  public static void main (String [] args) {
    String mySubject = thesisSentence();  // Initialize subject to pass on to other methods
    // Also print first sentence by calling method
    thesisSupport(mySubject);  // Call second method to make second sentence using subject
    conclusionSentence(mySubject);  // Call third method to make third sentence using subject
  }
}