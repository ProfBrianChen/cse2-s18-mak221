/// Draw Poker HW09
/// Marta Kasica-Soltan
/// April 13, 2018
/// CSE2S-210
/// Makes a poker game, automatically-generated, for two players

public class DrawPoker {
  // Make method to shuffle the deck
  public static int [] shuffle (){
    int array[] = new int[52];  // Declare and allocate an array w 52 spots
    // Create the basic deck, 0-51
    for (int a = 0; a < array.length; a++){
      array[a] = a;
    }
    int newArray[] = new int[52];  // Declare new array for shuffled version
    // Set values for new shuffled array
    for (int b = 0; b < array.length; b++){
      int c = (int)((Math.random()*51.999));  // Randomly set a target pos 0-51
      // If the value hasn't been taken already, take it
      if (array[c] != -1){
        newArray[b] = array[c];
        array[c] = -1;  // Then set value to -1
      }
      // Otherwise, continue through the loop once more
      else {
        b--;
      }
    }
    return newArray;  // Return shuffled "deck"
  }
  // Method to convert an int from the deck into "card terms"
  public static void cardConverter (int card){
    // Determine card value
    int cardName = card%13;  // Set cardName to value of card
    // Remember that the array starts at zero but cards begin at one
    switch (cardName) {
      case 0: System.out.print("the ace of ");  // Print out the name of the card
        break;  // Break
      case 1: System.out.print("the two of ");
        break;
      case 2: System.out.print("the three of ");
        break;
      case 3: System.out.print("the four of ");
        break;
      case 4: System.out.print("the five of ");
        break;
      case 5: System.out.print("the six of ");
        break;
      case 6: System.out.print("the seven of ");
        break;
      case 7: System.out.print("the eight of ");
        break;
      case 8: System.out.print("the nine of ");
        break;
      case 9: System.out.print("the ten of ");
        break;
      case 10: System.out.print("the jack of ");
        break;
      case 11: System.out.print("the queen of ");
        break;
      case 12: System.out.print("the king of ");
        break;
      default: System.out.print("");  // If none of these options, print nothing
        break;
    }
    // Determine card suit
    int cardSuit = card/13;
    // Card suits will be one of four
    switch (cardSuit) {
      case 0: System.out.print("diamonds");  // Print out the card suit
        break;  // Break
      case 1: System.out.print("clubs");
        break;
      case 2: System.out.print("hearts");
        break;
      case 3: System.out.print("spades");
        break;
      default: System.out.print("");
        break;
    }
  }
  // Make a method to tell you what the highest number in an array is
  public static int highestNumber (int [] array) {
    int highestNumber = 0;  // Highest number in array
    int cardName[] = new int[5];  // Declare and allocate for new array
    // Change hand into 0-12
    for (int a = 0; a < array.length; a++){
      cardName[a] = array[a]%13;  // Modular 13 returns 0-12
    }
    // If the value at a given spot is greater than current highest number, 
    for (int b = 0; b < array.length; b++){
      if (cardName[b] > highestNumber){
        highestNumber = cardName[b];  // change highest number
      }
    }
    return highestNumber;  // Return the highest number in array
  }
  // Checks for a pair
  public static boolean pair(int [] array) {
    boolean answer = false;  // Originally there is assumed no pair
    int cardName[] = new int[5];  // Make new array, same size
    // Change values in new array to be 0-12 of the old one
    for (int a = 0; a < array.length; a++){
      cardName[a] = array[a]%13;
    }
    // Set up two nested for statements that run through the new array twice
    // If these are not talking about the same spot but they have the same value,
    // There is assumed to be a pair
    for (int b = 0; b < cardName.length; b++){
      for (int c = 0; c < cardName.length; c++){
        if (b != c){
          if (cardName[b] == cardName[c]){
            answer = true;
          }
        }
      }
    }
    return answer;  // Return whether there is one
  }
  // Method for evaluating three of a kind's card number
  public static int threeOfAKindValue (int [] array) {
    int answer = -1;  // Which number is 3 of a kind
    int cardName[] = new int[5];  // Create new array for switching to 0-12
    // Change to 0-12 for card evaluation purposes
    for (int a = 0; a < array.length; a++){
      cardName[a] = array[a]%13;
    }
    // Set up three nested for statements that run through the new array three times
    // If these are not talking about the same spot but they have the same value,
    // There is assumed to be a three of a kind
    for (int b = 0; b < cardName.length; b++){
      for (int c = 0; c < cardName.length; c++){
        for (int d = 0; d < cardName.length; d++){
          if (b != c && c != d && b != d){
            if (cardName[b] == cardName[c] && cardName[c] == cardName[d]){
              answer = cardName[b];  // If they have the same value, there's a three of a kind
            }
          }
        }
      }
    }
    return answer;  // Return whether there is one
  }
  // Method for evaluating whether there is three of a kind
  public static boolean threeOfAKind (int [] array) {
    // Returns true if there's a three of a kind because it will say that
    // the value that has three is greater than or equal to zero, so true
    boolean answer = false;
    if (threeOfAKindValue(array) >= 0){
      answer = true;
    }
    return answer;
  }
  // Method for seeing whether there is a flush or not
  public static boolean flush (int [] array){
    boolean answer = false;  // Answer assumed to be false
    int cardSuit[] = new int[6];  // Create new array for changing all into suits
    for (int a = 0; a < array.length; a++){
      cardSuit[a] = array[a]/13;  // Returns 0-3 for the values
    }
    cardSuit[5] = cardSuit[4];
    // If the card suit at any point is not equal to the card suit at another,
    // it breaks out of the for loop that will otherwise continue
    for (int b = 0; b < array.length; b++){
      if (cardSuit[b] != cardSuit[b+1]){
        break;
      }
      // Once reaches final option, if this is true and loop is still going, all are true
      if (b > 3 && cardSuit[b] == cardSuit[b-1]){
        answer = true;
      }
    }
    return answer;  // Return whether there is a flush
  }
  // Method for evaluating for a full house
  public static boolean fullHouse (int [] hand){
    boolean answer = false;  // Assumed to be false
    int array[] = new int[5];  // Create new array for switching to 0-12
    // Change to 0-12 for card evaluation purposes
    for (int a = 0; a < hand.length; a++){
      array[a] = hand[a]%13;
    }
    // If there is a three of a kind
    if (threeOfAKind(hand) == true){
      int value = threeOfAKindValue(hand);  // Which card there are three of
      int remainder[] = new int[3];  // Used for all cards not in the three
      int b = 0;  // Counter for new array spots used
      for (int a = 0; a < array.length; a++){
        // If the value at a is not the three of a kind, move it to remainder
        if (array[a] != value){
          remainder[b] = array[a];
          b++;  // Increment for counting how many cards are in remainder
        }
      }
      // If the remaining two cards are the same, there is a full house
      if (remainder[0] == remainder[1]){
        answer = true;
      }
    }
    return answer;  // Return whether there is a full house
  }
  public static void main (String [] args){
    int array[] = shuffle();  // Deck of cards, shuffled
    int playerOne[] = new int[5];  // Hand for player one
    int playerTwo[] = new int[5];  // Hand for player two
    int oneCounter = 0;  // Counts how many cards player one has
    int twoCounter = 0;  // Counts how many cards player two has
    // Give hands to both players, one card at a time
    for (int b = 0; b < 10; b++){
      // Give evens to first player
      if (b%2 == 0){
        playerOne[oneCounter] = array[b];  // Assigns card to player one
        oneCounter++;  // Indicates that a card has been given
        System.out.print("Card "+oneCounter+" for player one is ");
        cardConverter(array[b]);  // Puts card value through method that prints which card it is
        System.out.println();  // New line
      }
      // Give odds to second player
      if (b%2 != 0){
        playerTwo[twoCounter] = array[b];  // Assigns card to player two
        twoCounter++;  // Indicates that a card has been given
        System.out.print("Card "+twoCounter+" for player two is ");
        cardConverter(array[b]);  // Puts card value through method that prints which card it is
        System.out.println();  // New line
      }
    }
    // Print out each player's hand
    System.out.print("Player one's hand is ");
    for (int c = 0; c < playerOne.length; c++){  // Prints out hand all together
      cardConverter(playerOne[c]);
      // Print commas except at the end
      if (c != 4){
        System.out.print(", ");
      }
    }
    System.out.println();  // New line
    System.out.print("Player two's hand is ");
    for (int d = 0; d < playerTwo.length; d++){  // Prints out second player's full hand
      cardConverter(playerTwo[d]);
      // Print commas except at the end
      if (d != 4){
        System.out.print(", ");
      }
    }
    // Check who wins
    boolean playerOneWins = false;  // Initially set to false for whether the player won
    boolean playerTwoWins = false;  // Initially set to false for player two
    // Checking for full house
    // Same basic concept for all nested if statements
    // Check if one player has a full house. If both do, tie. If one does, they win
    // If neither do, continue on the path of checking for the next tier below
    if (fullHouse(playerOne) == true && fullHouse(playerTwo) == false){
      playerOneWins = true;
    }
    else if (fullHouse(playerOne) == false && fullHouse(playerTwo) == true){
      playerTwoWins = true;
    }
    else if (fullHouse(playerOne) == true && fullHouse(playerTwo) == true){
      playerOneWins = true;
      playerTwoWins = true;
    }
    // Check for flush if neither won from previous
    if (playerTwoWins == false && playerOneWins == false){
      if (flush(playerOne) == true && flush(playerTwo) == false){
        playerOneWins = true;
      }
      else if (flush(playerOne) == false && flush(playerTwo) == true){
        playerTwoWins = true;
      }
      else if (flush(playerOne) == true && flush(playerTwo) == true){
        playerOneWins = true;
        playerTwoWins = true;
      }
      // Check for three of a kind if neither won from previous
      if (playerTwoWins == false && playerOneWins == false){
        if (threeOfAKind(playerOne) == true && threeOfAKind(playerTwo) == false){
          playerOneWins = true;
        }
        else if (threeOfAKind(playerOne) == false && threeOfAKind(playerTwo) == true){
          playerTwoWins = true;
        }
        else if (threeOfAKind(playerOne) == true && threeOfAKind(playerTwo) == true){
          playerOneWins = true;
          playerTwoWins = true;
        }
        // Check for pairs if neither won from previous
        if (playerTwoWins == false && playerOneWins == false){
          if (pair(playerOne) == true && pair(playerTwo) == false){
            playerOneWins = true;
          }
          else if (pair(playerOne) == false && pair(playerTwo) == true){
            playerTwoWins = true;
          }
          else if (pair(playerOne) == true && pair(playerTwo) == true){
            playerOneWins = true;
            playerTwoWins = true;
          }
          // Check for highest number  if neither won from previous
          if (playerTwoWins == false && playerOneWins == false){
            if (highestNumber(playerOne) > highestNumber(playerTwo)){
              playerOneWins = true;
            }
            else if (highestNumber(playerOne) < highestNumber(playerTwo)){
              playerTwoWins = true;
            } 
            // If neither have a number higher than the other, same number, both win
            else {
              playerOneWins = true;
              playerTwoWins = true;
            }
          }
        }
      }
    }
    // Print out final results
    System.out.println("");
    // If they both win, print draw
    if (playerOneWins == playerTwoWins){
      System.out.println("It's a draw!");
    }
    // If player one wins, print it. If player two won, print it.
    else if (playerOneWins == true){
      System.out.println("Player one wins!");
    }
    else if (playerTwoWins == true){
      System.out.println("Player two wins!");
    }
  }
}