/// Yahtzee HW04
/// Marta Kasica-Soltan
/// February 17, 2018
/// CSE2S-210
/// Scores a roll of a Yahtzee game

import java.util.Scanner;  // Import scanner class
// Main method required for every Java program
public class Yahtzee {
  public static void main(String[] args) {
    // Initialize scanner for incoming inputs
    Scanner myScanner = new Scanner(System.in);
    // Ask user whether they have randomly rolled dice or not
    System.out.print("Do you have five dice rolled already? 1 for yes, 0 for no: ");
    // Store response as int for if statements
    int questionAnswer = (int)myScanner.nextDouble();
    int firstDie = (int)0;  // Value of die 1
    int secondDie = (int)0;  // Value of die 2
    int thirdDie = (int)0;  // Value of die 3
    int fourthDie = (int)0;  // Value of die 4
    int fifthDie = (int)0;  // Value of die 5
    // Since someone answered that they have dice already, obtain values
    if (questionAnswer == 1) {
      // Ask for value on face of die, repeat for all five
      System.out.print("Please input the first number: ");
      // Store value of die as int, repeat for all five
      firstDie = (int)myScanner.nextDouble();
      // Use if statement to make sure int is between or including 1 and 6, repeat
      if (firstDie >= 1 && firstDie <= 6) {
        System.out.print("Please input the second number: ");
        secondDie = (int)myScanner.nextDouble();
        if (secondDie >= 1 && secondDie <= 6) {
          System.out.print("Please input the third number: ");
          thirdDie = (int)myScanner.nextDouble();
          if (thirdDie >= 1 && thirdDie <= 6) {
            System.out.print("Please input the fourth number: ");
            fourthDie = (int)myScanner.nextDouble();
            if (fourthDie >= 1 && fourthDie <= 6) {
              System.out.print("Please input the fifth number: ");
              fifthDie = (int)myScanner.nextDouble();
              if (fifthDie < 1 || fifthDie > 6){
                System.out.print("This number cannot be found on a die.");
              }
            }
            // Use else statement to indicate that this number is impossible
            else {
              System.out.print("This number cannot be found on a die.");
              // Program will exit. This else repeats for all five dice
            }
          }
          else {
            System.out.print("This number cannot be found on a die.");
          }
        }
        else {
          System.out.print("This number cannot be found on a die.");
        }
      }
      else {
        System.out.print("This number cannot be found on a die.");
      }
    }
    // Human answered that they do not have dice
    if (questionAnswer == 0) {
      // Give reassurance
      System.out.print("That's okay, we'll roll them for you.");
      // Randomly generate dice rolls and turn them into ints
      firstDie = ((int)Math.floor(Math.random()*6))+1;  // Randomly generated first die
      secondDie = ((int)Math.floor(Math.random()*6))+1;  // Randomly generated second die
      thirdDie = ((int)Math.floor(Math.random()*6))+1;  // Randomly generated third die
      fourthDie = ((int)Math.floor(Math.random()*6))+1;  // Randomly generated fourth die
      fifthDie = ((int)Math.floor(Math.random()*6))+1;  // Randomly generated fifth die
    }
    
    ///////////////////
    // UPPER SECTION //
    ///////////////////
    
    // Obtain total value for upper section by adding each face value
    int upperSectionInitialTotal = firstDie+secondDie+thirdDie+
      fourthDie+fifthDie;
    // Initialize upper with bonus outside of if statement
    int upperSectionIncludingBonus = 0;
    // Print the value for the upper section without bonus
    System.out.println("The initial total for the upper section is "+
                     upperSectionInitialTotal+".");
    // Check if upper section totals over 63, if it does, add 35 for bonus
    if (upperSectionInitialTotal >= 63) {
      upperSectionIncludingBonus = upperSectionInitialTotal+35;
    }
    // If not over 63, bonus value doesn't exist
    else {
      upperSectionIncludingBonus = upperSectionInitialTotal;
    }
    // Print the value for the upper section with bonus
    System.out.println("The total for the upper section including bonus is "+
                    upperSectionIncludingBonus+("."));
    
    ///////////////////
    // LOWER SECTION //
    ///////////////////
    
    // Initialize variables
    int addedOnes = 0;  // Amount of dice with 1
    int addedTwos = 0;  // Amount of dice with 2
    int addedThrees = 0;  // Amount of dice with 3
    int addedFours = 0;  // Amount of dice with 4
    int addedFives = 0;  // Amount of dice with 5
    int addedSixes = 0;  // Amount of dice with 6
    // Use switch statements to increment amount of
    // dice with given values. For ex, if firstDie 
    // has value 5, the number of fives aka addedFives
    // will increase by 1
    switch (firstDie) {
      case 1: addedOnes++;
        break;
      case 2: addedTwos++;
        break;
      case 3: addedThrees++;
        break;
      case 4: addedFours++;
        break;
      case 5: addedFives++;
        break;
      case 6: addedSixes++;
        break;
      // If value is not any 1-6, default
      default: addedOnes = addedOnes+0;
        break;
    }
    // Use switch statement to increase the
    // counter for dice value of secondDie
    switch (secondDie) {
      case 1: addedOnes++;
        break;
      case 2: addedTwos++;
        break;
      case 3: addedThrees++;
        break;
      case 4: addedFours++;
        break;
      case 5: addedFives++;
        break;
      case 6: addedSixes++;
        break;
      default: addedOnes = addedOnes+0;
        break;
    }
    // Use switch statement to increase the
    // counter for dice value of thirdDie
    switch (thirdDie) {
      case 1: addedOnes++;
        break;
      case 2: addedTwos++;
        break;
      case 3: addedThrees++;
        break;
      case 4: addedFours++;
        break;
      case 5: addedFives++;
        break;
      case 6: addedSixes++;
        break;
      default: addedOnes = addedOnes+0;
        break;
    }
    // Use switch statement to increase the
    // counter for dice value of fourthDie
    switch (fourthDie) {
      case 1: addedOnes++;
        break;
      case 2: addedTwos++;
        break;
      case 3: addedThrees++;
        break;
      case 4: addedFours++;
        break;
      case 5: addedFives++;
        break;
      case 6: addedSixes++;
        break;
      default: addedOnes = addedOnes+0;
        break;
    }
    // Use switch statement to increase the
    // counter for dice value of fifthDie
    switch (fifthDie) {
      case 1: addedOnes++;
        break;
      case 2: addedTwos++;
        break;
      case 3: addedThrees++;
        break;
      case 4: addedFours++;
        break;
      case 5: addedFives++;
        break;
      case 6: addedSixes++;
        break;
      default: addedOnes = addedOnes+0;
        break;
    }
    // Initialize variables outside of if statements
    int threeOfAKind = 0;  // Will be nonzero with three of a kind
    int fourOfAKind = 0;  // Will be nonzero with four of a kind
    int yahtzee = 0;  // Will be nonzero with five of a kind
    int chance = upperSectionInitialTotal;  // Chance means sum of dice values
    int fullHouse = 0;  // Will be nonzero with three of a kind and pair
    int largeStraight = 0;  // Will be nonzero with five sequential values
    int smallStraight = 0;  // Will be nonzero with four sequential values
    
    // Check for three of a kind
    // If exists, value for three of a kind
    // is the sum of the dice that make it up
    if (addedOnes == 3) {
      threeOfAKind = 3;  // Value of three since three ones
    }
    if (addedTwos == 3) {
      threeOfAKind = 6;
    }
    if (addedThrees == 3) {
      threeOfAKind = 9;
    }
    if (addedFours == 3) {
      threeOfAKind = 12;
    }
    if (addedFives == 3) {
      threeOfAKind = 15;
    }
    if (addedSixes == 3) {
      threeOfAKind = 18;
    }
    // Check for four of a kind
    // If exists, value for three of a kind
    // is the sum of the dice that make it up
    if (addedOnes == 4) {
      fourOfAKind = 4;  // Value of four since four ones
    }
    if (addedTwos == 4) {
      fourOfAKind = 8;
    }
    if (addedThrees == 4) {
      fourOfAKind = 12;
    }
    if (addedFours == 4) {
      fourOfAKind = 16;
    }
    if (addedFives == 4) {
      fourOfAKind = 20;
    }
    if (addedSixes == 4) {
      fourOfAKind = 24;
    }
    // Checks for whether all dice had same value
    // If true, additional 50 points will be added to score
    if (addedOnes == 5 || addedTwos == 5 || addedThrees == 5 ||
       addedFours == 5 || addedFives == 5 || addedSixes == 5) {
      yahtzee = 50;
    }
    // Checks for whether there was a three of a kind
    if (addedOnes == 3 || addedTwos == 3 || addedThrees == 3 ||
       addedFours == 3 || addedFives == 3 || addedSixes == 3) {
      // If previous passes, checks for whether there was a pair
      if (addedOnes == 2 || addedTwos == 1 || addedThrees == 2 ||
       addedFours == 2 || addedFives == 2 || addedSixes == 2) {
        fullHouse = 25;  // Given three of a kind and a pair, full house is found,
                         // value for full house added to total
      }
    }
    // For a large straight, values must be 1 for added twos, threes, fours, fives
    if (addedTwos == 1 && addedThrees == 1 && addedFours == 1 && addedFives == 1) {
      largeStraight = 40;  // If true, value for either sixes or ones must be 1.
                           // Either will result in large straight, thus points added
    }
    // For small straight, added threes and fours must have value 1
    if (addedThrees == 1 && addedFours == 1) {
      // Small straight of 1-2-3-4
      if (addedOnes == 1 && addedTwos == 1) {
        smallStraight = 30;
      }
      // Small straight of 2-3-4-5
      else if (addedTwos == 1 && addedFives == 1) {
        smallStraight = 30;
      }
      // Small straight of 3-4-5-6
      else if (addedFives == 1 && addedSixes == 1) {
          smallStraight = 30;
      }
    }
    // Calculate lower section total by adding all misc point options
    int lowerSectionTotal = threeOfAKind+fourOfAKind+yahtzee+fullHouse+
      largeStraight+smallStraight+chance;
    // Calculate total of game by adding lower and upper sections with bonus
    int total = lowerSectionTotal+upperSectionIncludingBonus;
    // Print lower section total
    System.out.println("The lower section total is "+
                      lowerSectionTotal+".");
    // Print combined total
    System.out.println("The whole total is "+total+".");
  }
}